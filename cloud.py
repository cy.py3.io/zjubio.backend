# coding: utf-8

from leancloud import Engine
from leancloud import LeanEngineError
from EasyLogin import EasyLogin
from app import app


engine = Engine(app)


@engine.define
def hello(**params):
    if 'name' in params:
        return 'Hello, {}!'.format(params['name'])
    else:
        return 'Hello, LeanCloud!'

import requests
import base64
import json
import Image
import io
@engine.define
def classify(**params):
    if "url" not in params:
        return {"status":"error"}
    url = params["url"]
    try:
        image_original_bytes = requests.get(url).content
        size = len(image_original_bytes)/1024
        print("Original size: {size}KB".format(size=size))
        upload_bytes = image_original_bytes
        while size>700:
            #fake IO to create Image
            fp_image = io.BytesIO(image_original_bytes)
            the_image = Image.open(fp_image)
            #resize the image
            weight,height = the_image.size
            small_image = the_image.resize((weight/2,height/2),Image.ANTIALIAS)
            #fake IO to get bytes from Image
            fp_small_image = io.BytesIO()
            small_image.save(fp_small_image,"jpeg")
            fp_small_image.seek(0)
            upload_bytes=fp_small_image.read()
            #for loop
            image_original_bytes = upload_bytes
            size = len(image_original_bytes)/1024
            print("After resize: {size}KB".format(size=len(upload_bytes)/1024))
        upload_base64_image = base64.b64encode(upload_bytes)
        data={"image":upload_base64_image+"\n","deviceid":"861337033853981","userid":""}
        x=requests.post("http://classify.nongbangzhu.cn/flowerclassify",headers={"Content-Type":"application/json; charset=UTF-8","User-Agent":"okhttp/2.5.0"},data=json.dumps(data)).json()["payload"]
        result = {
            "status": "ok",
            "is_plant": x["is_plant"],
            "description": x["list"][0]["description"],
            "latin": x["list"][0]["latin"],
            "name": x["list"][0]["name"],
            "pic": x["list"][0]["sample_pic_urls"][0]["pic_big"],
            "score":x["list"][0]["score"]
        }
        print(result)
    except Exception,e:
        result={"staus":"error","error":str(e)}
    return result

a=EasyLogin()

@engine.define
def gene_search(**params):
    global a
    if "keyword" not in params:
        return {"status":"error"}
    keyword = params["keyword"]
    a.get("http://www.wikigenes.org/search.html?search="+keyword,cache=True)
    a.d("div",{"class":"searchFormPlaceHolder"})
    a.d("div",{"id":"noscriptMsg"})
    frame = a.b.find("div",{"class":"main_content"})
    result = []
    for div in frame.find_all("div"):
        try:
            part={"url":"","title":"","species":"","content":""}
            part["url"]=div.find("a")["href"]
            part["title"]=" ".join([t.strip() for t in a.text(div.find("div",{"class":"queryResultLeft"}))])
            part["species"]=div.find("div",{"class":"queryResultRight"}).text
            part["content"]=div.find("div",{"class":"queryResultSnippet"}).text
            result.append(part)
        except:
            continue
    return result

@engine.define
def gene_content(**params):
    global a
    if "url" not in params:
        return {"status":"error"}
    url = params["url"]
    a.get("http://www.wikigenes.org/"+url,cache=True)
    a.d("div",{"id":"stub"})
    a.d("div",{"id":"stub_msg"})
    content = a.b.find("div",{"id":"artCntId"})
    result = []
    for block in content.find_all("h2"):
        result_part = {"title":"","content":""}
        result_part["title"]=block.text
        block_content = block.next_sibling
        if block_content is None:
            break
        t=0
        for line in block_content.find_all("li"):
            t+=1
            result_part["content"]+="  "+str(t)+". "+line.text+"\n"
        result.append(result_part)
        break # bad trim
        
    result_part = {"title":"","content":""}
    result_part["title"] = "Reference: "
    reference = a.b.find("div",{"class":"orgMememoir_cc_ref"})
    i = 0
    for line in reference.find_all("li"):
        i+=1
        result_part["content"]+="  %d. %s\n"%(i,line.text.replace("[Pubmed]",""))
        if i==t: break # bad trim
    result.append(result_part)
    return result

@engine.before_save('Todo')
def before_todo_save(todo):
    content = todo.get('content')
    if not content:
        raise LeanEngineError('内容不能为空')
    if len(content) >= 240:
        todo.set('content', content[:240] + ' ...')
